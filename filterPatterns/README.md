# Adiciona filtro de estampas no ProductsFilter

Altere os seguintes arquivos:

## liquids/partials/content/tag/_filters.liquid

Adicione as linhas abaixo dentro da tag `{% body %}`:

```{% load_tags types: 'estampa' %}
  <script> window.tagsPattern = {{ loaded_tags | to_json }} </script>```

## assets/javascripts/components/vndaComponents.js

Acima da definicão da variável `property2`, adicione as linhas abaixo:

```let patterns = window.tagsPattern.map(tag => ({ 
      value: 
        aggregations.properties.property2.filter(property => property.value.includes(tag.name))[0].value, 
      pattern: tag.image_url 
    })); ```

E adicione o parametro `patterns` no objeto que é adicionado na variável `property2`, conforme exemplo abaixo:

```let property2 = aggregations.properties.property2.length > 0 && {
      title: 'Cor',
      property: 'property2',
      style: 'colors',
      colorsProps: {
        showTitle: true,
        showColor: true,
      },
      options: aggregations.properties.property2,
      patterns: patterns,
    };```

# Como é feito o cadastro:

***Informações:***

| Dúvida                          | Instrução                                         |
| ------------------------------- | ------------------------------------------------- |
| **Onde cadastrar**              | Tags                                              |
| **Onde será exibido**           | Imagens das estampas dos produtos                 |
| **Cadastro exemplo em staging** | [Admin](https://template3.vnda.com.br/admin/tags) |

&nbsp;

***Informações sobre os campos***

| Campo         | Funcional?          | Orientação                                        |
| ------------- | ------------------- | ------------------------------------------------- |
| **Nome**      | :black_circle:      | Nome da tag precisa ser o mesmo que o mesmo inserido na variante |
| **Título**    | :large_blue_circle: | Inserir o texto que será exibido                  |
| **Subtítulo** | :no_entry:          |                                                   |
| **Descrição** | :no_entry:          |                                                   |
| **Tipo**      | :large_blue_circle: | `estampa`                                         |
| **Imagem**    | :no_entry:          | imagem da estampa                                 |

***Observações***

Nome da tag precisa ser o mesmo que o inserido na variante. Exemplo:

nome da tag = `estampa-xadrez`
Variante = `Xadrez | estampa-xadrez`;