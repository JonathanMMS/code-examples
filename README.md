# Code Examples Vnda

Repositório com exemplos de código de referências para parceiros.

- [Menu com icones](https://gitlab.com/vnda/code-examples/-/tree/main/menu-icons)
- [Guia de medidas](https://gitlab.com/vnda/code-examples/-/tree/main/simple-size-guide)
- [Guia de medidas com variação por tamanhos](https://gitlab.com/vnda/code-examples/-/tree/main/size-guide-2)
- [Descrições de produtos com descrição curta e collapses](https://gitlab.com/vnda/code-examples/-/tree/main/description-with-collapses)
- [Produtos relacionados](https://gitlab.com/vnda/code-examples/-/tree/main/relateds)
- [Produtos personalizados](https://gitlab.com/vnda/code-examples/-/tree/main/customizations)
- [Assinatura (compra recorrente) de produtos](https://gitlab.com/vnda/code-examples/-/tree/main/assinatura)
- [Kits (adição ao carrinho de vários produtos em uma mesma requisição)](https://gitlab.com/vnda/code-examples/-/tree/main/kits)
- [Breadcrumbs (produto e listagem)](https://gitlab.com/vnda/code-examples/-/tree/main/breadcrumbs)
- [Cálculo de frete na página de produto](https://gitlab.com/vnda/code-examples/-/tree/main/shipping)
- [Integração com o Instagram](https://gitlab.com/vnda/code-examples/-/tree/main/instagram)
- [Compre o feed](https://gitlab.com/vnda/code-examples/-/tree/main/compre-o-feed)
- [Conjunto (biquini)](https://gitlab.com/vnda/code-examples/-/tree/main/conjunto)
- [Estampas no componente de filtro](https://gitlab.com/vnda/code-examples/-/tree/main/filterPatterns)