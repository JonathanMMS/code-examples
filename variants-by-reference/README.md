# Variacões de produto por referencia (produtos diferentes)

Altere os seguintes arquivos:

## liquids/partials/content/product/variants/_variants.liquid

Adicione o seguinte trecho de código no inicio do arquivo:

```
  {% assign current_product = product %}
  {% assign variants_by_reference = '' %}

  {% for tag in product.tags %}
    {% if tag.type == 'modelo' %}
      {% load_products tag: tag.name per_page: '20' %}
      {% capture variants_by_reference %}
        {% for product in products %}
          {% assign attribute = product.variants[0].properties.property1.value %}
          
          {% if attribute != blank %}
            <a aria-label="{{ tag.name }}" href="{{ product.url }}" class="label {% if product.id == current_product.id %} selected {% endif %}">
              {% if attribute contains '|' and attribute contains '#' %}
                {% render 'partials/components/check_color', color: attribute %}
              {% else %}
                {% assign html_estampa = ''%}
                {% assign attr_down = attribute | split: '| ' | last | downcase %}
                {% assign tooltip = attribute | split: '|' | first %}
                {% for tag in product.tags  %}
                  {% if tag.type == "estampa" and attr_down == tag.name %}
                    {% capture html_estampa %}
                      <span class="color estampa" style="background-image: url({{ tag.image_url | resize: '30x' }});">
                        <span class="tooltip">{{ tooltip }}</span>
                      </span>
                    {% endcapture %}
                  {% endif %}
                {% endfor %}
                {% if html_estampa != blank %}
                  {{ html_estampa }}
                {% else %}
                  <span class="text">{{ attribute }}</span>
                {% endif %}
              {% endif %}
            </a>        
          {% endif %}
        {% endfor %}
      {% endcapture %}
      {% break %}
    {% endif %}
  {% endfor %}
```

E altere o render de `partials/content/product/variants/attributes` para passar a variável `variants_by_reference` para a property necessária, conforme exemplo abaixo:

```
  {% render 'partials/content/product/variants/attributes', attributes: attributes_1, attributes_name: attributes_1_name, attributes_index: 1, random: random, product: product, tagName:tagName, variants_by_reference: variants_by_reference %}
```

## liquids/partials/content/product/variants/_attributes.liquid 

Dentro da div .inner, printar a variável `variants_by_reference` conforme exemplo:

```
  <div class=" {% if select %}custom-select{% endif %}" data-produto="{{ attributes_name }}-attribute">
    ...
    {{ variants_by_reference }}
  </div>
```

# Como é feito o cadastro:

***Informações:***

| Dúvida                          | Instrução                                         |
| ------------------------------- | ------------------------------------------------- |
| **Onde cadastrar**              | Tags                                              |
| **Onde será exibido**           | Variacões do produto por referencia               |
| **Cadastro exemplo em staging** | [Admin](#)                                        |

&nbsp;

***Informações sobre os campos***

| Campo         | Funcional?          | Orientação                                        |
| ------------- | ------------------- | ------------------------------------------------- |
| **Nome**      | :black_circle:      |                                                   |
| **Título**    | :large_blue_circle: |                                                   |
| **Subtítulo** | :no_entry:          |                                                   |
| **Descrição** | :no_entry:          |                                                   |
| **Tipo**      | :large_blue_circle: | `modelo`                                          |
| **Imagem**    | :no_entry:          |                                                   |
