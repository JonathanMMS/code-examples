# Compra liberada apenas para usuários logados e aprovados

Essa funcionalidade é muito utilizada para lojas b2b, onde os buyers passam por uma aprovacão por parte do seller, e, apenas depois de aprovados, podem realizar compras na loja.

Antes de realizar a implementacão, é necessário ter algumas informacões:

1. quais páginas da loja devem ser acessíveis sempre e quais páginas devem ser acessíveis apenas quando logado?
2. se as páginas de listagem e produto forem acessíveis sem login, o preco deve ser visível ou apenas se o cliente estiver logado?
3. se o preco for visível sem login, ele é o mesmo que o preco apresentado quando logado?

Com essas respostas, o desenvolvedor conseguirá realizar a implementacão conforme as necessidades do cliente.

**Informacões importantes:**

* A verificacão e aprovacão do buyer é feita pelo seller de forma manual.
* O buyer é cadastrado automáticamente na loja, porém ele fica com aprovacão pendente, e só poderá acessar as páginas restritas após aprovado.
* Para aprovar o buyer, o seller deve adicionar a tag `aprovado` no usuário no painel administrativo.
* Enquanto o buyer estiver com aprovacão pendente, pode-se implementar uma tela de "aguardando aprovacão", com um formulário de contato para que ele possa pedir informacões.

## Verificacão de usuário logado e/ou aprovado

Para verificarmos se o usuário está logado, usamos a variável logged_in - ela retorna `true` se o usuário estiver logado e `falso` se estiver deslogado. Quando o usuário está logado, vamos preencher a variável cli com as informacões do cliente.

É importante usar essa variável auxiliar `cli` pois a variável `client` não existe se o usuário não estiver logado, e isso pode ocasionar erros no liquid.

```
{% assign cli = false %}
{% if logged_in %}
  {% assign cli = client %}
{% endif %}
```

Em seguida, para validar as informacões que devem ser mostradas no ecommerce, verificamos utilizando a variável `cli` - se ela existe e se o atributo `tags` contém a tag aprovado, conforme exemplo a seguir:

```
{% if logged_in and cli.tags and cli.tags contains 'aprovado' %}
    // conteúdo que deve ser mostrado apenas para usuários logados E aprovados
{% endif %}
```

Se for necessário mostrar algum aviso para o usuário "aguardando aprovacão", pode-se validar apenas se ele está logado, conforme abaixo:

```
{% if logged_in and cli.tags %}
  {% unless cli.tags contains 'aprovado' %}
    // conteúdo que deve ser mostrado apenas para usuários logados E NÃO aprovados
  {% endunless %}
{% endif %}
```

No caso da loja inteira ser acessível apenas com acesso logado, essa validacão pode ser feita no arquivo `layout.liquid`, como exemplo a seguir:

```
{% if logged_in and cli.tags and cli.tags contains 'aprovado' %}
  <main>
    {{ yield }}
  </main>
{% endif %}
```

**Importante:**

- Nossas páginas tem cache, e por isso as variáveis do liquid podem não atualizar automaticamente quando o login é feito e o usuário acessa uma página acessada recentemente. Para resolver esse problema, podemos utilizar o endpoint `/conta/cliente` para mostrar ou ocultar as informacões conforme a necessidade, segue exemplo:

```
<script>
  async function getClient () {
    const response = await fetch('/conta/cliente', {
      method: 'GET',
      cache: 'reload'  
    })

    return response.json();
  }
  getClient().then((data) => {
    //console.log('getClient data', data); 
    if (data.email) {
      // executa acão caso cliente estiver logado
    } else {
      // executa acão caso cliente NÃO estiver logado
    }
  }) 
</script>
```

## Precos diferentes quando logado ou deslogado

É possível ter promocões aplicadas a clientes com tags especificas (ver artigo http://ajuda.vnda.com.br/pt-BR/articles/4000157-promocao-do-tipo-aplicada-a-clientes), e para que essa promocão seja aplicada na vitrine da loja, deve ser feita a seguinte alteracão:

```
{% logged_in and cli.tags and cli.tags contains 'aprovado' %}
  <strong>{{ product | client_price | money_format }}</strong>
{% else %}
  <strong>{{ product.price | money_format }}</strong>
{% endif %}

```