# Compre o conjunto

Primeiramente, copie os arquivos dessa pasta para o projeto, seguindo as instrucões abaixo:

- o arquivo groupShop.js deve ser adicionado na pasta assets/javascripts/components
- o arquivo _groupShop.scss deve ser adicionado na pasta assets/sass/content/product
- a pasta group_shop (que contém os arquivos liquid) deve ser adicionado na pasta liquid/partials/components

Em seguida, deve ser feito as seguintes alteracões:

## liquids/partials/content/product/infos/_infos.liquid

Adicione a seguinte verificacão no inicio do arquivo:

```{% assign conjunto = false %}
{% for tag in product.tags %}
  {% if tag.type == "conjunto" %}
    {% assign conjunto = true %}
    {% assign classConjunto = 'conjunto' %}
  {% endif %}
{% endfor %}```

Em seguida, adicione a lógica para, caso o produto seja um conjunto, mostrar a sessão de conjunto, se não, deve aparecer o formulário de compra "normal", como exemplo abaixo:

```{% if conjunto %}
    {% render 'partials/components/group_shop/group_shop_area', product: product, current_product: current_product, current_shop: current_shop %}
  {% else %}
    <div class="product-price">
      <div data-was-processed="false" 
        data-sku="" data-init-price=
        '{ 
          "productId": {{ product.id }}, 
          "showInstallmentsWithoutInterest": true, 
          "showInstallmentsWithInterest": true, 
          "mode": "long",
          "htmlTag": "h2" 
        }'
      ></div>
    </div>

    {% render 'partials/content/product/purchase', product: product, show_qtd_selector: true, tag_medidas: tag_medidas %}
    {% render 'partials/content/product/form_notify', product: product, current_shop: current_shop %}
  {% endif %}```

## assets/sass/pages/product_above.scss

Importe o arquivo _groupShop.scss, conforme abaixo:

```@import "../content/product/groupShop";```

## assets/javascripts/pages/product.js

Importe o arquivo groupShop.js, conforme abaixo:

```import GroupShop from './components/groupShop';```

E inicialize o GroupShop no evento `DOMContentLoaded`:

```GroupShop.init();```
