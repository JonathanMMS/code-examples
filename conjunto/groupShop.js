import CartDrawer from '../common/cartDrawer';
import Store from './store';
import { formatMoney } from './utilities';

import Swiper, { Navigation, Pagination } from 'swiper';
Swiper.use([Navigation, Pagination]);

const GroupShop = {
  products: document.querySelectorAll('[data-prod-group-shop]'),
  selectedProducts: [],

  page: document.querySelector('body').getAttribute('data-page'),

  //Marca/desmarca o produto para adição
  handleSelection: function (currentProduct) {
    var _this = this;
    const { selectedProducts, addItemToBuy, removeItem } = GroupShop;

    // console.log('currentProduct', currentProduct );
    if (GroupShop.page == 'product') {
      setTimeout(()=>{
        let sku_currentProduct = currentProduct.querySelector('input').value;
        // console.log('cuurentProduct sku ', sku_currentProduct )
        GroupShop.setImagesGroupProduct(sku_currentProduct)
      }, 50)
    }


    if (selectedProducts.length > 0) {
      const match = selectedProducts.filter((product) => {
        return product.ref == currentProduct.getAttribute('data-prod-ref');
      });

      if (match.length == 0) {
        addItemToBuy(currentProduct);
      } else {
        if (!currentProduct.classList.contains('active')) removeItem(currentProduct);
      }
    } else {
      // Sem produto nenhum, adicionar normalmente o primeiro
      addItemToBuy(currentProduct);
    }

    if (document.querySelector('body').classList.contains('page-product')) {
      if (selectedProducts.length == 1) {
        document.querySelector('#selected-products').innerHTML = `Selecionado: ${selectedProducts.length} produto`;
      } else {
        document.querySelector('#selected-products').innerHTML = `Selecionado: ${selectedProducts.length} produtos`;
      }
    }
  },
  setImagesGroupProduct: function (_sku = '') {
    var $mainImages = $('[data-section-product]').find('[data-product-images] [data-main-slider]');
    var $thumbsImages = $('[data-section-product]').find('[data-product-images] [data-slider-thumbs]');
    var slideProduct = window.sliderProduct[0];
    var $imagesWithoutSkus = $mainImages.find('[data-skus-image=","]');
    var $thumbsWithoutSkus = $thumbsImages.find('[data-skus-image=","]');
    
    var $allSkuImage = $mainImages.find("[data-image][data-skus-image]")
    var $allSkuImageThumb = $thumbsImages.find("[data-image-thumb][data-skus-image]")

    // // console.info('$allSkuImage', $allSkuImage);
    // console.info('$allSkuImageThumb', $allSkuImageThumb);

    // // console.info('$mainImages', $mainImages);
    // console.info('$thumbsImages', $thumbsImages);
    // console.info($mainImages.find('[data-skus-image*="' + _sku + ',"]'));
    // console.info('[data-skus-image*="' + _sku + '"]');

    // Esconde as imagens que não contém o sku
    $mainImages
      .find("[data-image][data-skus-image]")
      .removeClass("active")
      .addClass("inactive");

    $thumbsImages
      .find("[data-image-thumb][data-skus-image]")
      .removeClass("active")
      .addClass("inactive");

    // Mostra as imagens que contém o sku
    // $mainImages
    //   .find('[data-image][data-skus-image*="' + _sku + ',"]')
    //   .addClass("active")
    //   .removeClass("inactive");

    // $thumbsImages
    //   .find('[data-image-thumb][data-skus-image*="' + _sku + ',"]')
    //   .addClass("active")
    //   .removeClass("inactive");
    
    
    // function findSkuImages(Images) {
    //   for(var i of Images) {

    //     var firstSkuImage = i.dataset.skusImage.split(',')[0];

    //     // console.log('_sku.includes(i.dataset.skusImage)', _sku.includes(firstSkuImage))
        
    //     if(_sku.includes(firstSkuImage)) {
    //       i.classList.add("active");
    //       i.classList.remove("inactive");
    //       // console.log('entrou no if adiciona active e tira inactive')
    //     } else {
    //       i.classList.add("inactive");
    //       i.classList.remove("active");
    //       // console.log('entrou no else remove active e adicona inactive')
    //     }
    //   }
    // }
    function findSkuImages(Images) {
      for(var i of Images) {

        // var firstSkuImage = i.dataset.skusImage.split(',')[0];

        // console.log('_sku.includes(i.dataset.skusImage)', _sku.includes(firstSkuImage))

        var SkusImage = i.dataset.skusImage.split(',');
        // console.log('SkusImage', SkusImage)
        for(var skuImage of SkusImage) {
          // console.log('_sku', _sku)
          if (skuImage != "") {
            if(_sku.includes(skuImage) ) {
              // console.log('skuImage:', skuImage)
              // console.log('_sku.includes(i.dataset.skuimage)', _sku.includes(skuImage))

              i.classList.add("active");
              i.classList.remove("inactive");
              // console.log('entrou no if adiciona active e tira inactive')
              break
            } else {
              i.classList.add("inactive");
              i.classList.remove("active");
              // console.log('entrou no else remove active e adicona inactive')
            }
          }
        }
      }
    }

    findSkuImages($allSkuImageThumb);

    // As imagens sem skus selecionados são exibidas sempre
    // $imagesWithoutSkus.removeClass("inactive").addClass("active");
    // $thumbsWithoutSkus.removeClass("inactive").addClass("active");

    slideProduct.slideTo($('[data-image-thumb].active').index('[data-image-thumb]:visible'));
    slideProduct.update();
  
  },
  // Adiciona item na array para adição futura
  addItemToBuy: function (product) {
    const ref = product.getAttribute('data-prod-ref');
    const price = Number(product.getAttribute('data-price'));
    const salePrice = Number(product.getAttribute('data-sale-price'));
    const available = product.getAttribute('data-available');
    const discountPercent = product.getAttribute('data-discount-percent');
    const generalSku = product.getAttribute('data-sku-product-general')
    product.classList.add('active')

    // Adiciona produto na array
    GroupShop.selectedProducts.push({
      productEl: product,
      ref,
      price,
      salePrice,
      available,
      discountPercent,
      generalSku,
    });

    // Atualiza Preço
    GroupShop.updateFinalPrice();
  },
  // Remove item da array para adição futura
  removeItem: function (product) {

    for (let index = 0; index < GroupShop.selectedProducts.length; index++) {
      const selectedProduct = GroupShop.selectedProducts[index];

      if (selectedProduct.ref == product.getAttribute('data-prod-ref')) {
        // Muda sinalização do botão
        const button = product.querySelector('.add-to-group');
        if (button) button.classList.remove('-group-selected');
        // product.parentElement.classList.remove('active')

        // Remove produto da array de adição
        GroupShop.selectedProducts.splice(index, 1);
        break;
      }
    }

    // Atualiza Preço
    GroupShop.updateFinalPrice();
  },
  // Atualiza exibição de valores
  updateFinalPrice: function () {
    const { selectedProducts } = GroupShop;
    const purchaseWrapper = document.querySelector('.group-shop .purchase');

    if (selectedProducts.length == 0) {
      if (purchaseWrapper) purchaseWrapper.classList.add('-hidden');
    } else {
      document.querySelector('.group-shop .purchase').classList.remove('-hidden');

      let priceTotal = 0.0;
      let salePriceTotal = 0.0;
      let discountTotal = 0;
      let unavailableProducts = 0;

      // Soma os valores dos produtos selecionados, já considerando se tem desconto informado
      //e se a variante escolhida está disponível ou não

      selectedProducts.forEach((product) => {
        const prodPrice = parseFloat(product.productEl.getAttribute('data-price'));
        const salePrice = parseFloat(product.productEl.getAttribute('data-sale-price'));
        const available = product.productEl.getAttribute('data-available');

        if (available === 'false') {
          unavailableProducts++;
        } else {
          if (prodPrice != salePrice) {
            priceTotal = priceTotal + prodPrice;
            salePriceTotal = salePriceTotal + salePrice;
          } else {
            priceTotal = priceTotal + prodPrice;
            salePriceTotal = salePriceTotal + prodPrice;
          }
        }
      });

      if (priceTotal !== salePriceTotal) {
        discountTotal = 100 - (salePriceTotal * 100) / priceTotal;
      }

      // Exibe os valores finais calculados em tela
      const priceEl = document.querySelector('.group-shop .purchase .price-group');
      const totalProducts = selectedProducts.length - unavailableProducts;

      if (priceEl) priceEl.innerText = formatMoney(salePriceTotal);


      if (discountTotal != 0) {
        document.querySelector('.discount-wrapper').classList.remove('-hidden');

        const originalPriceEl = document.querySelector('.group-shop .purchase .original-price');
        const discountEl = document.querySelector('.group-shop .purchase .discount-percent');
        const warning = document.querySelector('.group-shop .purchase .warning');
        originalPriceEl.innerText = formatMoney(priceTotal);
        discountEl.innerText = `-${discountTotal.toFixed(2)}%`;

        if (unavailableProducts > 0) {
          warning.innerText = `Atenção! ${unavailableProducts} ${unavailableProducts > 1 ? 'produtos estão indisponíveis' : 'produto está indisponível'
            } `;
        } else {
          warning.innerText = '';
        }
      }
    }
  },
  // Adiciona os produtos selecionados e o produto principal
  addProducts: async function (generalSku) {
    const btnComprar = document.querySelector('[data-group-shop-add]');
    const mainForm = document.querySelector('[data-form-product-group-shop]');

    let data = {
      items: [],
    };
    

    // Produtos selecionados no compre junto
    for (let index = 0; index < GroupShop.selectedProducts.length; index++) {
      const product = GroupShop.selectedProducts[index].productEl;
      const sku = product.querySelector('input[name="sku"]').value;
      const available = product.getAttribute('data-available');
      const productGeneralSku = product.getAttribute('data-sku-product-general');

      // Para o compra rapida funfar certinho esses dois abaxo tem que ter valor igual.
      // console.log('generalSku', generalSku)
      // console.log('productGeneralSku', productGeneralSku)

      if (productGeneralSku != null) {
        if (available === 'true' && productGeneralSku == generalSku) {
          data.items.push({
            sku: sku,
            quantity: product.querySelector('[name="quantity"]').value,
          });
        } else {
          if (available === 'true') {
            data.items.push({
              sku: sku,
              quantity: product.querySelector('[name="quantity"]').value,
            });
          }
        }
      }
    }

    const json_data = JSON.stringify(data);
    // Produto principal da página
    const purchaseWrapper = document.querySelector('.purchase');
    const boxResponse = purchaseWrapper.querySelector('.msg-response');

    if (!btnComprar.classList.contains('-adding')) {
      btnComprar.classList.add('-adding');

      try {
        const response = await fetch('/carrinho/adicionar/kit', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: json_data,
        });

        if (response.ok) {
          // console.log('success');
          const html = await response.text();
          Store.addItemResult('produto-adicionado', html, boxResponse, mainForm);
          CartDrawer.show()

          if (document.querySelector('body').classList.contains('page-product')) {
            if (data.items.length == 1) {
              setTimeout(() => {
                document.querySelector('.popup-group-product').classList.add('active')
              }, 1000);
            }
          }

          purchaseWrapper.classList.remove('error');
          // loader.classList.remove('-active');
        } else {
          console.log('error');
          purchaseWrapper.classList.add('error');
          console.error(`Erro ao adicionar os produtos do compre junto. Status ${response.status}`);

          Store.addItemResult('erro-adicionar', null, boxResponse, mainForm);
          // loader.classList.remove('-active');
        }
      } catch (error) {
        console.error('Erro ao enviar os produtos do compre junto.');
        console.error(error);
        Store.addItemResult('erro-adicionar', null, boxResponse, mainForm);
        // loader.classList.remove('-active');
      }

      btnComprar.classList.remove('-adding');
    }
  },
  setCarrosselProductBlocks: function () {
    const productsBlock = document.querySelectorAll('.product-block');

    productsBlock.forEach(item => {
      const product = item.querySelector('.swiper');
      const next = item.querySelector('.swiper-button-next');
      const prev = item.querySelector('.swiper-button-prev');

      const newProductCarousel = new Swiper(product, {
        slidesPerView: 1,
        spaceBetween: 0,
        watchOverflow: true,
        navigation: {
          nextEl: next,
          prevEl: prev,
        },
      });
    });
  },
  init: function () {
    this.setCarrosselProductBlocks()
    const { products, handleSelection, addProducts } = GroupShop;

    if (products.length > 0) {
      products.forEach((product) => {
        // Adiciona todos os produtos no compre junto no load da loja
        product.querySelectorAll('.attributes .option-Cor input').forEach((el, i) => {
          el.addEventListener('change', () => {
            handleSelection(product);
          })
        });
        
        product.querySelectorAll('.attributes .option-Tamanho input').forEach((el, i) => {
          el.addEventListener('change', () => {
            handleSelection(product);
          })
        });

        if (product.querySelector('.remove-group')) {
          product.querySelector('.remove-group').addEventListener('click', () => {
            product.classList.remove('active')
            handleSelection(product);
            product.querySelector('[name="attribute-Tamanho"]:checked').checked  = false
          })
        }

        // Lida com o evento de atualização de preço
        product.addEventListener('vnda:group-shop-price-update', () => {
          GroupShop.updateFinalPrice();
        });
      
      });

      // Lida com o botão de adicionar os produtos do compre junto no carrinho
      
      const ButtonsGroupShop = document.querySelectorAll('[data-group-shop-add]');
      
      ButtonsGroupShop.forEach( (button) => {
        button.addEventListener('click', () => {
          let generalSku = button.getAttribute('data-sku-product-general');
          addProducts(generalSku);
        });
      });
    }
  },
};

export default GroupShop;
